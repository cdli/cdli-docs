---
title: Setting-up a user
section: User guides
category: Features guide
audiences:
  - Content contributor
chips:
  - Administrator
authors:
  - Pagé-Perron, Émilie
---

## Setting up a user to contribute
  
### Author entry
First, verify if there is an existing author entry for this person. Well published Assyriologists will have an entry, students probably not. Go to https://cdli.mpiwg-berlin.mpg.de/authors and search for the person's name. If you cannot find them, log in, then click on the "+ Add Author" button to the right top of the page on the Authors index. 
    
![Add author on desktop](/cdli-docs/images/Screenshot_2023-09-07_at_11.10.26.png) 
    
![Add author on smaller screen](/cdli-docs/images/Screenshot_2023-09-07_at_11.10.35.png) 
    
Fill in the details about the author, only information the author is happy to share online. If in any doubt, only give their full name and, id available, their ORCID. In the biography field, you can add a link to their online profile on their favourite platform.
    
Submit the form to save the new author.
    
### Account creation
Indicate to the person that they need to create an account on the cdli website. The url is https://cdli.mpiwg-berlin.mpg.de/register but one can find the link visible on the main page on the right top of the screen. 
      
![register](/cdli-docs/images/Screenshot_2023-09-07_at_11.17.16.png) 
    
There is documentation available for the part of the registration where the user needs to set up two step authentication for all platforms: https://cdli.mpiwg-berlin.mpg.de/docs/two-factor-guide.
    
### Activating crowdsourcing privilege
After the user have created their account and let you know, or you find out they have done so by looking at the list of users, you need to activate their crowdsourcing privilege. In order to do so, first find ther user entry in the list of users. You must first be logged in and go to the Admin Dashboard https://cdli.mpiwg-berlin.mpg.de/admin/dashboard   
    
Admin Dashboard > Users and Staff > Users > Index.  
    
![User Index](/cdli-docs/images/Screenshot_2023-09-07_at_11.27.26.png)   
  
In the user index, you can order the entries by clicking on the headings in the table. If you sort by "Created On" (click twice for more recent at the top), you will find the most recent entries. Click on the Edit button to the right of your user. This will open the edit form for this user. In the form, scroll down and chose the appropriate author from the author list. The aurthors are sorted in alphabetical order ands the list is very long so please be patient and keep scrolling until you find the right author. 
  
![User Index](/cdli-docs/images/Screenshot_2023-09-07_at_11.32.32.png)   
  
After chosing the auhor, click "Save".
  
In order for the user to gain their crowdsourcing privilege, they must now log out and log back in into their account.

### Sample email for your potential user

Please register an account on the CDLI website at https://cdli.mpiwg-berlin.mpg.de/register and let me know when you have done so. I will then activate your crowdsourcing privilege.   
  
If you face any difficulties in setting up the two factor authentication, please refer to this documentation page: https://cdli.mpiwg-berlin.mpg.de/docs/two-factor-guide. If you have any question or if you face any issue in the process, please write to cdli-support@ames.ox.ac.uk.


