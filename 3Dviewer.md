---
title: View 3D models
section: User guides
category: Features guide
audiences:
  - Content contributor
chips:
  - Visitor
authors:
  - Dhar, Mustafa
  - Pagé-Perron, Émilie
---
## Find and see a 3D model
To find a 3D model, do an empty search and filter your results to "3D model" under "Data", "Artifact images".  
![flash for redirect to view](/cdli-docs/images/Screenshot 2023-07-14 at 16.14.52.png)  
You can also simply look up this model: https://cdli.mpiwg-berlin.mpg.de/artifacts/215518/reader/232490

## Features
The list below explains each feature available in the 3D viewer. The list is in the same order as the icons appearing on the viewer.  

![flash for redirect to view](/cdli-docs/images/Screenshot 2023-07-14 at 16.21.15.png)

- `Rotate` Will allow 3D model to rotate using the mouse or touch(for touchscreens) movement
- `Move` Model can be moved from its place
- `Zoom In` With this, view can be zoomed in for an enlarged view of the model
- `Zoom Out` Zoom out for a less enlarged view of the model
- `Undo` The last rotation or movement is undone
- `Reset` All rotations and movements are undone, reseting the model to its starting position
- `Lighting` The direction of the virtual lighting effect can be moved using the mouse
- `Fullscreen` Sets the height and width of the 3D Viewer to the full screen view port
