---
title: REST API
section: User guides
category: API usage
audiences:
  - Visitor
  - Developer
chips:
  - api
authors:
  - Willighagen, Lars
---

Most APIs work through [Content Negotiation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Content_negotiation). Every API type lists the MIME types used for this. In some cases, regular file extensions are listed. They can be used as an alternative.

# Metadata

Regular metadata export is enabled for almost everything. The schema is described [here](/pages/schema/1.0). The following formats are supported:

| Format | MIME type        | File extension |
|--------|------------------|----------------|
| JSON   | application/json | .json          |

The following paths are supported:

  * GET `/abbreviations/*`
  * GET `/agadeMailsCdliTags/*`
  * GET `/agadeMails/*`
  * GET `/archives/*`
  * GET `/articlesAuthors/*`
  * GET `/articlesPublications/*`
  * GET `/articles/*`
  * GET `/artifactsCollections/*`
  * GET `/artifactsComposites/*`
  * GET `/artifactsDates/*`
  * GET `/artifactsExternalResources/*`
  * GET `/artifactsGenres/*`
  * GET `/artifactsLanguages/*`
  * GET `/artifactsMaterials/*`
  * GET `/artifactsPublications/*`
  * GET `/artifactsSeals/*`
  * GET `/artifactsShadow/*`
  * GET `/artifacts/*`
  * GET `/artifactsUpdates/*`
  * GET `/artifactTypes/*`
  * GET `/authorsPublications/*`
  * GET `/authors/*`
  * GET `/authorsUpdateEvents/*`
  * GET `/cdliTags/*`
  * GET `/collections/*`
  * GET `/dates/*`
  * GET `/dynasties/*`
  * GET `/editorsPublications/*`
  * GET `/editors/*`
  * GET `/entryTypes/*`
  * GET `/externalResources/*`
  * GET `/genres/*`
  * GET `/inscriptions/*`
  * GET `/journals/*`
  * GET `/languages/*`
  * GET `/materialAspects/*`
  * GET `/materialColors/*`
  * GET `/materials/*`
  * GET `/months/*`
  * GET `/periods/*`
  * GET `/postings/*`
  * GET `/postingTypes/*`
  * GET `/proveniences/*`
  * GET `/publications/*`
  * GET `/regions/*`
  * GET `/retiredArtifacts/*`
  * GET `/roles/*`
  * GET `/rolesUsers/*`
  * GET `/rulers/*`
  * GET `/signReadingsComments/*`
  * GET `/signReadings/*`
  * GET `/staff/*`
  * GET `/staffTypes/*`
  * GET `/updateEvents/*`
  * GET `/users/*`
  * GET `/years/*`

# Linked data

The CDLI catalog supports Linked Open Data output for catalog data. The following formats are supported:

| Format    | MIME type                                             | File extension |
|-----------|-------------------------------------------------------|----------------|
| JSON-LD   | application/ld+json                                   | .jsonld        |
| RDF/JSON  | application/rdf+json                                  | \-             |
| RDF       | application/rdf+xml                                   | .xml, .rdf     |
| N-Triples | application/n-triples                                 | .nt            |
| Turtle    | text/turtle, application/turtle, application/x-turtle | .ttl           |

The following paths, part of the main catalog, support linked data APIs:

  * GET `/archives/*`
  * GET `/artifacts/*`
  * GET `/artifactsExternalResources/*`
  * GET `/artifactsMaterials/*`
  * GET `/collections/*`
  * GET `/dates/*`
  * GET `/dynasties/*`
  * GET `/genres/*`
  * GET `/inscriptions/*`
  * GET `/languages/*`
  * GET `/materials/*`
  * GET `/materialAspects/*`
  * GET `/materialColors/*`
  * GET `/periods/*`
  * GET `/proveniences/*`
  * GET `/publications/*`
  * GET `/regions/*`
  * GET `/rulers/*`

# Bibliographies

Bibliography exports are supported for artifacts and publications. The following formats are supported:

| Format                   | MIME type                               | File extension |
|--------------------------|-----------------------------------------|----------------|
| BibTeX                   | application/x-bibtex                    | .bib           |
| CSL-JSON                 | application/vnd.citationstyles.csl+json | \-             |
| Formatted bibliographies | text/x-bibliography                     | \-             |
| RIS                      | application/x-research-info-systems     | .ris           |

Formatting is done using [citeproc-js](http://citeproc-js.readthedocs.io/) and [CSL styles](https://citationstyles.org/). To specify the bibliographic style of the formatted bibliographies, use the `style` query parameter. For styles that can be used, check out [the CSL styles repository](https://github.com/citation-style-language/styles) or [Zotero's search index](https://www.zotero.org/styles).

The following paths are supported:

  * GET `/artifacts/*`
  * GET `/publications/*`

# Inscriptions

Inscriptions can be fetched from specific inscription versions or from the latest inscription of a given artifact. Note that CoNLL-RDF is included in the [linked data export](#linked-data). The following formats are supported:

| Format     | MIME type         | File extension |
|------------|-------------------|----------------|
| C-ATF      | text/x-c-atf      | \-             |
| CDLI-CoNLL | text/x-cdli-conll | \-             |
| CoNLL-U    | text/x-conll-u    | \-             |

The following paths are supported:

  * GET `/artifacts/*/inscription/*`
  * GET `/inscriptions/*`

# Tabular exports

Tabular exports are supported for artifacts, publications, and the links between the two (artifactsPublications). The following formats are supported:

| Format | MIME type                 | File extension |
|--------|---------------------------|----------------|
| CSV    | text/csv                  | .csv           |
| TSV    | text/tab-separated-values | .tsv           |
| Excel  | application/vnd.openxmlformats-officedocument.spreadsheetml.sheet | .xlsx |

The following paths are supported:

  * GET `/artifacts/*`
  * GET `/artifactsPublications/*`
  * GET `/publications/*`
