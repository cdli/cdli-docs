---
title: Adding and Editing Links Between Metadata Concepts (Entities) and Publications
section: Contribution and data guides
category: Features guide
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Chen, Circle
  - Willighagen, Lars
---

Publications can be linked to artifacts and related topics. There are three ways to do
this:

  1. By [editing the artifacts](add-and-edit-artifact-metadata).
  2. By editing the related topic, e.g. going to a provenience and clicking the "Edit"
     button.
  3. In bulk, by uploading a CSV file.

## Bulk upload

To upload any kind of topic in bulk, you can upload a CSV file. It can have the
following columns:

  - `bibtexkey` (*required*): the BibTeX key of the publication
  - `entity_table` (*required*): the type of topic to link with, e.g. `artifacts` or `proveniences`
  - `entity_id` (*required*): the numeric identifier of the topic, usually visible in the URL,
     e.g. "123" for [P000123](https://cdli.mpiwg-berlin.mpg.de/artifacts/123)
     or "21" for [Ur (mod. Tell Muqayyar)](https://cdli.mpiwg-berlin.mpg.de/proveniences/21).
  - `publication_type` (*required*): what the publication is to the topic: e.g. the `primary`
     publication. Other allowed values are: `history`, `electronic`, `citation`, `collation`, and `other`.
  - `exact_reference`: the exact page, plate number, etc. where the topic is referenced.
  - `publication_comments`: any comments.
