---
title: Bibliographic Data Management
section: Contribution and data guides
category: Features guide
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
  - Chen, Circle
---

## Preamble notes
- All bibliographic data and their links to artifacts and other entities (such as proveniences, periods, etc.) are fully open and publicly accessible.

- At this time, only administrators and editors have the privilege to manage all aspects of the bibliographic data. Contributors should write to CDLI staff regarding required changes to bibliographic entries for now. However you can use these instructions to prepare data files to send to the administrators and editors.

- Links between publications and artifacts can be suggested by anyone with crowdsourcing privilege through the artifacts update feature.


## Bibliographic data structure and fields

The bibliographic format employed at CDLI is the BiBTeX format, with additional fields for your convenience. You can read more about the BiBTeX format on the [official BiBTeX website](http://www.bibtex.org/) and at the [BiBTeX Wikipedia entry](https://de.wikipedia.org/wiki/BibTeX).

 Notable is our usage of the BiBTeX ´note´ field to store the Assyriological abbreviation of the publication (the equivalent of the BiBTeX key but specific to the field of Assyriology) which is mapped in the CDLI database to the "designation" of the publication.

 <div class="alert alert-info">
 Tip: To decipher the Assyriological abbreviation of a publication, use the [CDLI abbreviation list]( https://cdli.mpiwg-berlin.mpg.de/abbreviations).
 </div>

For a complete description of all formats and fields we use for publication data, see [Bibliography index and search](/docs/CDLI-bibliography#content-understanding-bibliographic-data)

## Adding bibliographic entries to the CDLI database

### Authors and journals
Before you can submit your data, authors, editors and journals mentioned in your references need to be added to the database first. If you cannot find your author here https://cdli.mpiwg-berlin.mpg.de/authors, please send an email to cdli so we can add it for you. Journal titles are available here: https://cdli.mpiwg-berlin.mpg.de/journals. Simply write to us if you would like an entry added.

### Add a publication using the form (single publication)
To add a new publication, you can use this form: https://cdli.mpiwg-berlin.mpg.de/admin/publications/add. Don't forget to fill the BiBTeX key and designation.

### Add publications using bulk upload (single or multiple publications)
To upload publications in bulk, prepare your data using your favourite reference management software. You must use the standard BiBTeX fields. You can use the ´note´ field to share the assyriological abbreviation. Export your data in CSV format including all fields.   
To submit your file, use this form: https://cdli.mpiwg-berlin.mpg.de/admin/publications/add/bulk. If the data is not conform, you will get a warning, so you can edit and try again. The best way to prepare a file for import is to export existing data and use the headers from that file, and the data in each field as a working example. 

## Editing bibliographic data
At this time there is no way to bulk edit publication data. This means each entry needs to be edited individually. Simply click on the edit button visible on the publication page of the publication you wish to edit. To find a publication, refer to [Bibliography index and search](/docs/CDLI-bibliography#Bibliography-index-and-search)

### Adding and editing links between metadata concepts (entities) and publications
#### Adding and editing links between artifacts and publications
##### Add through the interface
###### From the artifact
On the artifact's page, spot the ´link publications´ grey button and click on it.  
  
![image](/cdli-docs/images/bib01.png)  
  
On the Add Entity-Publication Link page, scroll to the ´link publication´ section.   
  
![image](/cdli-docs/images/bib03.png)  
  
Add the bibtexkey of the publication, the exact reference (page number, text number) and chose which type of publication this is (primary, history, citation, collation). Then click submit.

###### From the publication
On the artifact's page, spot the ´link artifacts´ grey button and click on it.  
  
![image](/cdli-docs/images/bib02.png)  
  
On the Add Entity-Publication Link page, scroll to the ´Link artifacts to this publication´ section.  
  
![image](/cdli-docs/images/link-artifact.png)  
  
Add the artifact id, that is the P number without the P. Also add the exact reference (page or text number) and the type of publication.
#### Edit through the interface
###### From the artifact
On the artifact's page, spot the ´link publications´ grey button and click on it.  
  
![image](/cdli-docs/images/bib01.png)    
  
Scroll down to the section where you see the list of linked publication.   
  
![image](/cdli-docs/images/edit-linked-publications.png)  
  
Select which link you would like to edit or delete.  

###### From the publication
On the artifact's page, spot the ´link artifacts´ grey button and click on it.  
  
![image](/cdli-docs/images/bib02.png)  
  
Scroll down to the section where you see the list of linked artifacts.  
  
![image](/cdli-docs/images/edit-linked-artifacts.png)  
  
Select which link you would like to edit or delete.  

##### Add or edit links between artifacts and publications through artifact update
A simple and efficient way to add or edit one or multiple links is through the bulk artifact metadata update. For this, you need to prepare a CSV file with the following fields:   artifact_id, publications_key, publications_type, publications_exact_ref, publications_comment. There is no need to provide other fields.  
Upload your file here: https://cdli.mpiwg-berlin.mpg.de/artifacts-updates/add. Select the option "Concatenate"   
  
![image](/cdli-docs/images/concatenate.png)  
  
If you would like to simply add publication information to the existing one, else the publication information for those artifacts will be overridden.  
  
Before your data is fully submitted, the framework will tell you if your data is conform and let you re-check it.
For more information on the artifact bulk upload see [link to come soon!].   

#### Adding and editing links between publications and other entities
Before doing anything, please make sure you are logged in with your CDLI account.

As admin user, one could do the following.

(1) Add links between any publication and artifact

Go to Admin Dashboard -> Entities-Publication Links -> Add New.

![image](/cdli-docs/images/add-edit/add-edit-1.PNG)

It would redirect to a form to fill in the artifact (or other type of entity) information and publication information. The link could then be added from there. Please make sure to select the type of entity from the form.

![image](/cdli-docs/images/add-edit/add-edit-2.PNG)

(2) Add linked artifacts to a selected publication

By going to the single view page for any publication, one can find "Link Artifacts" Button at the bottom.

![image](/cdli-docs/images/add-edit/add-edit-3.PNG)

Clicking on the link artifacts would redirect to the add new link form with the publication information already filled.

![image](/cdli-docs/images/add-edit/add-edit-4.PNG)

In the future one would be able to add other linked entities.

(3) Add linked publications to a selected artifact.

This is like mirror operation of (2). By going to the single view page for any artifact, one can expand the "Related Publications" tab and see the "Link Publications" button.

![image](/cdli-docs/images/add-edit/add-edit-5.PNG)

Clicking on it would redirect to the add new link form with the artifact information already filled.

![image](/cdli-docs/images/add-edit/add-edit-6.PNG)

In the future one would be able to add linked publications from other types of entities.

## Merging publications
Because of the nature of the data from the previous CDLI database, we need to manually curate bibliographic entries to prevent data loss and incorrct data. This has resulted in the creation of more publication entries than there should be, and we developped a system to enable easy merging of those entries when necessary.   
  
To merge publications, one must first visit the merge-select publication index and search page, here: https://cdli.mpiwg-berlin.mpg.de/admin/publications/merge-select. It functions in the same way as the search / index for publications but with added functionalities.  
1. Search for the publicatons to merge
2. Select the publications to merge, either by using the 'Select all' button, or by individually selecting publications using the checkboxes under 'Select'
3. If you have a preference, chose which entry you would like to be the master entry for this merge (chose the entry woth most details)
4. Click on 'Merge"
5. In the top section, verify that you have selected the entries you wanted to merge
6. In the bottom section, carefully edit the "excact reference" for the link betweem the artifact and the publication. This should be the page number or text number in the publication.
7. Click on "Submit"
8. You will arrive on the publication page; now click on edit to edit the publication
9. Update at least the designation of the publication, add more details if possible  
![image](/cdli-docs/images/exact_ref.png)  

  
  Now everyone can enjoy a fully curated publication: 
  - When clicking on the publication from the artifact, all artifacts linked to this publication will be listed
  - It will be easier to search for the publication because there will be more details in the publication entry 
