---
title: Searching the CDLI catalog
section: User guides
category: Search guide
audiences:
  - Visitor
chips: []
authors:
  - Willighagen, Lars
  - Pagé-Perron, Émilie
---

There are three ways to search in the catalog of artifacts of the CDLI: simple search, advanced search, and using the URL to select IDs
search.

1. [Simple search](#content-simple-search) is accessible from the home page and under "Search" in the navigation bar at the top of each page.
2. [Advanced search](#content-advanced-search) is available by clicking on the "Advanced search" link below the simple search field on the home page, and is also in the navigation bar at the top of the page.
3. [URL ID query](#content-url-id-query) is available using the URL bar of your browser.

## Simple search

> https://cdli.mpiwg-berlin.mpg.de/search

Simple search makes it possible to search in a number of domains:

- **Free Search** searches in all fields.
- **Publications** searches in the title, year, author name and more of the publications associated with the artifact.
- **Collections** searches in the names and locations of the collections that own or keep the artifacts.
- **Proveniences** searches in the locations where artifacts were found.
- **Periods** searches in the periods from which the artifacts were dated.
- **Transliterations** searches in the transliterations of the cuneiform.
- **Translations** searches in the translations of the cuneiform.
- **Identification numbers** searches in the various numbers used to identify artifacts, such as CDLI numbers, museum numbers, and references to publications.

You can select the domain with the dropdown to the right of the search field. Text entered in the search field is interpreted in the same way as in [advanced search](#content-advanced-search).

**Multiple search queries** can be combined by clicking on "Add search field". The new query can be in a different domain than the previous domain. With the small dropdown to the left of the search field, it is possible to control whether artifacts should match both queries ("and") or if all artifacts that match either query should be returned ("or"). In a query like "(1) and (2) or (3)", "and" takes precedence over "or", so the artifacts that are returned that either match (3), or both (1) and (2).

<div class="alert alert-warning">
Note: If JavaScript is disabled in the browser, only one search query can be entered.
</div>

## Advanced search

> https://cdli.mpiwg-berlin.mpg.de/search/advanced

Advanced search allows for more detailed search queries. The form lists various different fields in which can be searched individually. It also allows some settings that affect [transliteration searches](#content-transliteration-searches). Individual searches are interpreted in one of four ways, as [described below](#content-search-field-behavior).

### Multiple searches

In advanced search, fields can contain **multiple searches**. To search for alternatives, combine values by placing `%OR%` between them. For example, `Uruk %OR% Larsa` searches for artifacts with a provenience of Uruk *or* Larsa. To combine two searches, place `%AND%` between them.

## Search field behavior

1. If the search is enclosed in double quotes (`"`) it is interpreted as-is. This can also be used to search for searches containing commas.
2. Else, if the search is enclosed in forward slashes (`/`) it is interpreted as a [RegExp](https://en.wikipedia.org/wiki/Regular_expression) pattern.
3. Else, if the search contains question marks or asterisks (`?` or `*`), the search is interpreted as a wildcard search. This means question marks can match any single character, and asterisks can match any number of characters.
4. Else, the search is interpreted differently depending on the field.
    - CDLI/artifact number can be searched with both "P000123" and "123". The same goes for composite numbers and seal numbers, though the starting letter is of course different ("Q" and "S" respectively).
    - Other identification numbers that contain abbreviations followed by a number (many museum, accession, and excavation numbers as well as references to artifacts in publications) can be searched with or without spaces between the abbreviation and the number, and with any amount of leading zeros before the number.
    - Some fields that contain diacritics are normalized. This includes proveniences, archives, referenced dates, author names, and more. If the search does not contain diacritics it will match documents both with and without the diacritics, otherwise only documents with those diacritics are returned.
    - The `created` and `modified` fields contain a date and a time, and can be [searched with ranges](#content-range-search).
    - [Transliteration searches](#content-transliteration-searches) get a special treatment altogether.
    - Other fields are not interpreted differently.

In all cases, searches are case-insensitive, meaning that uppercase searches can return lowercase results and vice versa.

### Transliteration search

Transliteration search behaves differently to account for the intricacies of the [ATF](http://oracc.museum.upenn.edu/doc/help/editinginatf/primer/index.html) file format.

- Searches are automatically bound to sign names, so searching for `ku` does not return `ku3`.
- Although CDLI uses `sz`, `s,`, `t,`, and `h` to represent `š`, `ṣ`, `ṭ` and `ḫ` respectively, the latter can also be used in searches, as well as `sh` and `c` as alternatives for `š` and `j` for `g`.
- Flags (`#`, `!`, `?`, `*`) are ignored when searching, unless they are explicitly searched for, meaning `ku` will also return `ku#?` but `ku#?` will not return just `ku`.

<div class="alert alert-info">
Tip: If you need to avoid this behavior, you can wrap the search query in double quotes (<code>"</code>) or use wildcard search.
</div>

There are also a number of options to further specify the behavior of transliteration search. These options can be set below the "Transliteration" field in [advanced search](#content-advanced-search).

- **Single line search** allows you to search for multi-word sequences that appear on a single line, instead of spanning multiple lines.
- **Case sensitive search** adds the requirement that the case of the results (uppercase/lowercase) matches that of the query. This is not the case by default.
- **Sign permutation search** transforms the search query to sign names, and returns results where a sequence of alternative readings matching those sign names is found. With this option activated, word boundaries are not taken into account.

### Range search

A range search is composed of one or more comparisons, i.e. `>`, `>=`, `<`, and `<=` (respectively more than, more than or equal to, less than, and less then or equal to). These comparisons can be against number or date fields. For example, for artifacts created in May 2024, the following range could be constructed: [`>=2024-05-01 <2024-06-01`](https://cdli.mpiwg-berlin.mpg.de/search?created=%3E=2024-05-01%20%3C2024-06-01). Alternatively, for artifacts with a P number between 1 and 100,000: [`>=1 <=100000`](). The following date and number formats are supported:

- Date and time (`YYYY-MM-DDThh:mm:ss`)
- Date only (`YYYY-MM-DD`, e.g. `2001-01-01`). This is interpreted as midnight (`00:00:00`) on that day.
- Whole number (`#`)
- Decimal number (`#.###`)

<div class="alert alert-warning">
Note: When constructing date ranges, beware that e.g. <code>2024-05-31</code> is interpreted as the very start of that day. This means that a query like <code>&gt;=2024-05-01 &lt;=2024-05-31</code> would exclude any artifacts created on May 31st, except in that first second of <code>00:00:00</code>. Similarly <code>&gt;2024-04-30 &lt;2024-06-01</code> would include most artifacts created on April 30th.
</div>

<div class="alert alert-warning">
Note: There are no number fields that support range search yet.
</div>

## URL ID query

In the URL bar of your browser, you can input a series of IDs to fetch multiple artifacts to view in search results. This will only work if all if your IDs are valid. Accepted IDs are:

- Q numbers
- S numbers
- P numbers

After the backslash that follows the cdli domain, add each ID you require and separate them with a comma, using no spaces. For example: https://cdli.mpiwg-berlin.mpg.de/P123456,S000001,P000001. The artifacts will appear on the search result page.

