---
title: Submitting artifact data for journal editors
section: Contribution and data guides
category: Editor guide
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
---
This documentation page addresses the steps a journal or edited volume editor might want to go through in order to make sure artifacts discussed in the articles or chapters they publish are clearly referenced in the cdli for users to find use in their research.  

## Minimal dataset

Association between an article and an artifact, with the exact reference (eg "p. 3"):  

This could take the form of a file named according to the article reference and which would contain a column with the CDLI P number (without the P), museum number, or known publication number and another column with the exact reference for that artifact in the article. Example:   

| artifact_id | exact_reference   |
|-------------|-------------------|
| 123456      | p. 3, no. 002     |
| 234567      | pp. 3-5, BM 06543 |



Basic metadata of artifacts that are not yet in databases:  
language, period, provenience, genre, collection, museum/accession number  

| languages         | period                    | provenience          | collections                                                                   | museum_no      | genres         |
|-------------------|---------------------------|----------------------|-------------------------------------------------------------------------------|----------------|----------------|
| Akkadian;Sumerian | Ur III (ca. 2100-2000 BC) | Nippur (mod. Nuffar) | Department of Near Eastern Studies, Cornell University, Ithaca, New York, USA | CUNES 02-33-24 | Administrative |

For multiple values, add a semicolon.  

Lists of entities (languages, periods, etc...) are available here: https://cdli.mpiwg-berlin.mpg.de/browse  

For a quick model, download any artifact in csv format and replace the data with your own values, eg https://cdli.mpiwg-berlin.mpg.de/artifacts/453608/csv   


BiBTeX file of the articles to add (most reference software can export to the BiBTeX format).  In the "How published" field, you can add the URL to the article in full eg "https://www.degruyter.com/document/doi/10.1515/za-2022-0001/html", we can then facilitate the navigation from the cdli artifact entry to the article. We use the "note" field to store the Assyriological abbreviation for a publication (eg. ZA 109).

## Ideal dataset

An ideal dataset would comprise all data related to an artifact, including photos, line art, comments on surface preservation, etc. However some information is more important than others for CDLI, the most important being the metadata. I outline each type of data below and number them by importance:


### 1. Artifacts Metadata

Detailed Instructions on preparing artifact metadata for CDLI, either adding new entries or editing existing ones are available here:

https://cdli.mpiwg-berlin.mpg.de/docs/add-and-edit-artifact-metadata


### 2. Artifact - publication link   


Example table:   

| artifact_id | publications_key | exact_reference | publication_type | museum_no      | genres         |
|-------------|------------------|-----------------|------------------|----------------|----------------|
| 1           | Sallaberger2008  | p. 3            | primary          | CUNES 02-33-24 | Administrative |
| 532123      | Dahl2011         | p. 22 no. 004   | citation         |                |                |


- artifact_id: the P number without the P  
- publications_key: bibliographic key of the article  
- exact_reference: the exact spot where one can find information about the artifact in the article  
 -reference_type: the CDLI type of reference: primary, history, citation, collation  


### 3. Bibliography  

The bibliography can be provided in BiBTeX format, and the BiBTeX keys should match the BibTeX key in the 2) artifact-publication link.    


At this time, only editors can insert bibliographic data. We will be happy to process such data and insert it for you. Once we have finished our update of the bibliography module, instructions will become available for self-service upload of bibliographic data, we will let you know then in case you would like to use it.   


### 4. Images   

If you or your authors have images to share, please let us know and we can provide instructions on processing or we can process them ourselves. We prefer 600 PPI tiff but can handle any image quality and format.   


### 5. Text  

ATF text should conform to the C-ATF format (http://oracc.museum.upenn.edu/doc/help/editinginatf/cdliatf/index.html) and can be submitted on the website.  


### 6. External resources  

External resources links can be provided as such:  


| external_resources | artifact_id | external_resources_key |
|--------------------|-------------|------------------------|
| i3.MesopOil        | 453608      | 3775                   |
| 532123             | Dahl2011    | p. 22 no. 004          |



## Data submission  
To submit data to the cdli on the website, the user needs to be registered and have their crowdsourcing privilege activated. When logged in, such user can either edit any artifact metadata, text, or textual annotations using the edit button or by submitting files at these addresses:  



Add or edit a group of artifact(s) https://cdli.mpiwg-berlin.mpg.de/artifacts-updates/add   [file upload].
Add one or more text(s), or annotation(s) https://cdli.mpiwg-berlin.mpg.de/inscriptions/add [form and file upload].

The review queue is accessible here: https://cdli.mpiwg-berlin.mpg.de/update-events  

If anyone from your team would like to submit data to cdli, please ask them to create an account on the platform, they can then write to me so I activate their crowdsourcing privilege. We will also slowly work our way into preparing documentation. In the meantime, we are absolutely happy to provide extra instructions for any operation you would like to perform.    

We are of course also glad if you simply send us the files by email which we can process to integrate the data ourselves.   

If you have any questions or doubts please do not hesitate to contact me. I'm very happy to provide more details.     
