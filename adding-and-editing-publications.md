---
title: Adding and editing bibliographical info
section: Contribution and data guides
category:
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Willighagen, Lars
---

Bibliographical information of publications related to artifacts and related topics can be
added in three ways:

  1. Individual publications can be added or edited in a [web form](#web-form).
  2. Multiple publications can be added or edited [in bulk](#bulk-upload) by uploading a
     spreadsheet or a BibTeX file.
  3. Duplicate publication entities can be [merged](#merging).

To edit associations between publications and artifacts or other related topics, see the guide
on [Adding and Editing Links Between Metadata Concepts (Entities) and Publications](adding-and-editing-entities-publications).

After submitting changes to bibliographical info, site administrators or content editors
will review the changes and if all is well approve them, updating the information visible
on the site. The revision history is available by going to a publication page and clicking
on "History" in the top right.

## Web form

To add a new publication, go to https://cdli.mpiwg-berlin.mpg.de/publications/add. To edit
an existing publication, go to the page belonging to the publication and click on the "Edit"
button in the top right. This will bring you to a web form were bibliographical information
of the publication can be added or updated. In general, the following fields should be filled
in if possible:

  - **Entry Type**: Whether the publication is a book, a chapter, a (journal) article, or something
    else.
  - **BibTeX Key** (*required*): A shorthand identifier for the publication, usually the last name
    of the author and the year of publication. If the identifier is not unique, you can try appending
    the first word of the title. Make sure not to include special characters.
  - **Designation**: A domain-specific shorthand for the publication, for example the abbreviation
    of the book series followed by the volume number in that series ("ATU 3").
  - **Title** (*required*): The title of the publication. See also the entry type-specific instructions
    below.
  - **Year** and **Month**: The year and month of publication. If available, the day of publication
    can be added in the month field.
  - **Authors**: Authors can be added by pressing the "Add Author" button.
  - **How Published**: The link to the publication can be included here.

### Books

The following fields are particularly relevant for books:

  - **Publisher**
  - **Address**: Country (and city) of the publisher.
  - **Editors**
  - **External Resources**: To add an **ISBN**, click "Add External Resource", select
    "International Standard Book Number" in the left dropdown, and enter the ISBN in the
    right field. Preferably, use a ISBN with 13 digits and hyphens, e.g. "978-90-90-08876-1".

For a single volume of a multi-volume book which itself is contained in a book series,
use **Book Title** and **Volume** for the multi-volume book, and **Series** and **Number**
for the book series.

For titled chapters, use **Title** for the title of the chapter and **Book Title** for the
title of the encompassing book. Optionally, **Chapter** can be used for the chapter number,
and **Pages** for the page range.

### Journal articles

The following fields are particularly relevant for journal articles:

  - **Journal**: If the journal is not available, please contact site administrators.
  - **Volume** and **Number**: Volume and issue number of the article.
  - **Pages**
  - **External Resources**: To add a **DOI**, click "Add External Resource", select
    "Digital Object Identifier" in the left dropdown, and enter the DOI in the
    right field. Only enter the DOI itself, not the full link, so e.g. "10.1111/icad.12730".

## Bulk upload

To add or edit publications in bulk, you can upload a CSV or BibTeX file at
https://cdli.mpiwg-berlin.mpg.de/publications/upload.

### Comma-Separated Values (CSV)

You can upload a CSV (spreadsheet) file. They are in the same format as the files that can
be downloaded from the publication search results. As an example, you can download
the following file: https://cdli.mpiwg-berlin.mpg.de/publications?from=2022&limit=10&format=csv.

### BibTeX

You can also upload a [BibTeX](https://en.wikipedia.org/wiki/BibTeX) file. Make sure that the
names in the `author`, `editor`, and `journal` field exactly match those in the CDLI database.
You can check https://cdli.mpiwg-berlin.mpg.de/authors for author and editor names, and
https://cdli.mpiwg-berlin.mpg.de/journals for journal names. If an author or a journal is missing,
please contact a site administrator. A designation can be added using the non-standard `designation`
field.

## Merging

To merge duplicate publications, go to https://cdli.mpiwg-berlin.mpg.de/publications/merge?step=select
and perform a search. Then, select exactly which publications you want to merge, and which publication
is the merge target (the other publications will be deleted in favour of the merge target). You will
then be prompted to update the artifacts that were linked to these publications, and optionally to edit
the bibliographical information of the merge target.
