---
title: Adding and editing CDLJ & CDLB articles
section: User guides
category: Editor guide
audiences:
  - Organization
authors:
  - Willighagen, Lars
---

This is a step-by-step guide for journal editors to add an article for CDLJ or CDLB.

  1. Start at https://cdli.mpiwg-berlin.mpg.de/admin/articles/add. Select the right journal and click <kbd>Continue</kbd>.
  2. After that, a form should show up. Enter the title, year, and number of the article.

  > Note that the year and number cannot be changed after the article is saved.

  3. Add the authors by searching for their name in the text field labeled "Search for Authors". Below the field, results will show up, each with a button labeled <kbd>Add</kbd>. Clicking this button will add the author to a list above the search field, with a button labeled <kbd>Remove</kbd> to remove the author from the list.

  > If an author does not show up in the search results, they can be added by going to https://cdli.mpiwg-berlin.mpg.de/admin/authors/add.

  Optionally, the current affiliation and email of the author can be added by clicking on the text <kbd>Additional info</kbd> and filling in the text fields.

  > If no affiliation or email is entered, the info attached to the author record are displayed instead. However, if those change the information shown with the article will not match the PDF.

  4. Upload the PDF by clicking the button labeled <kbd>Choose file</kbd> under the heading "Upload PDF" and selecting the file from your computer.

  5. Add the LaTeX by clicking the button labeled <kbd>Choose file</kbd> in the LaTeX tab under the heading "Article content". Alternatively, the LaTeX can be added by pasting it in the text field labeled "Paste LaTeX" and pressing the button labeled <kbd>Generate HTML</kdb>.

  6. Go to the "References" tab and upload the BibTeX file by clicking the button labeled <kbd>Choose file</kbd> and selecting the file from your computer. Then, go back to the "LaTeX" tab and press the button labeled <kbd>Generate HTML</kdb>.

  > If something does not seem to work in step 5 or 6, go to the "Logs" tab to see if there is more information there.

  7. Go to the "Images" tab and upload images by clicking the buttons labeled <kbd>Choose file</kbd> and select the respective files.

  8. Go to the "HTML" tab and check and/or edit the resulting HTML.

  > When the article is saved, the end result can look slightly different to what it looks like in the "HTML" tab. In addition, images are not displayed in the "HTML" tab.

  9. Enter the correct creation and modification dates in the respective date fields.

  > The creation date is shown as the date of publication.

  10. Set the article status. Drafted will make it available only to journal editors; Proofing will additionally make it available to CDLI user accounts that are linked to the author records of this article.

  11. Press the <kbd>Submit</kbd> button.
