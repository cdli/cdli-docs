---
title: CDLI Bibliographic Data
section: User guides
category: Features guide
audiences:
  - Visitor
  - Content contributor
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
---

## Consulting Bibliographic data
### Exploring bibliographic data
#### Bibliography index and search
The link to the bibliography is available under "Resources" in the main menu and is called "CDLI Bibliography". The link (https://cdli.mpiwg-berlin.mpg.de/publications) brings users to the Publications page, which is both the index and a search page, where one can find bibliographic references.  

If your search does not yield any result, try one keyword in the designation field, this will help match uncurated bibliographic entries.  

#### Artifact bibliography
On a single artifact page, like https://cdli.mpiwg-berlin.mpg.de/P123456 for example, when scrolling down to the "Related publications" accordion and clicking on the title, the accordion will open and show the list of linked bibliographic entries.   

Upon clicking on one entry, you can see the page of this publication and any artifacts related to it.  

### Downloading bibliographic data 
You can download references related to your search ("Export" button) or to a specific artifact, on their page ("Export artifact" button). You can download references from the publications index too ("Export Publications" button). 

### Understanding bibliographic data
#### Bibliographic data fields
All the bibliographic data fields are the exact same as BiBTeX except in some cases described below. See https://www.bibtex.com/format/ for a description of each field. Please give it a read to help us keep the data as clean as possible. For instance, 'book title' should be used when the publication is of @incollection or @inproceedings types only, and it is an obligatory field for those two publication types. There are many specificies like this one in the BiBTeX format.  
  
##### bibtexkey
The BiBTeX key is a universal, human friendly ID for publications. It holds the same role as the Assyriological publication abbreviation but is recognizable by indiviudals outside the field. It is also this data point that we use when connecting artifacts with publications. If you are preparing data to import in CDLI, try to use first author last name, year, one key word, all in one word. eg.: Englund1988timekeeping.


##### author and editor
In CDLI, when uploading bibliographic data, those fields are pluralized, as opposed to the BiBTeX format in which they are singular. Update these field names before submitting data in CSV format to cdli.   
  
When downloading bibliographc data from CDLI, in the BiBTeX format, the fields will be BiBTeX compliant, but the CSV version will have the pluralized verson of these fields.  

Authors should always be written the same way as in the CDLI author entry. For example "Jagersma, Abraham H.". When there are many authors, separate them using a semi-colon, like this: "Abaslou, Sina; Zamani, Amir". 
    
To search for an author's name, see here: https://cdli.mpiwg-berlin.mpg.de/authors. If you can't find an author, write to cdli@ames.ox.ac.uk to request the addition of the author in question.
  
Editors follow the same rules.  

##### how_published
If the publication is available on the internet, you can add the url in this field. It is also fine to add the doi url here.


##### journal
The journal name must come from this list: https://cdli.mpiwg-berlin.mpg.de/journals. If the journal you want to use is not in the list, write to cdli@ames.ox.ac.uk to request the addition of the journal in question.

##### note (BiBTeX only)
When we will offer upload of bibliographic data in BiBTeX format (for now we only offer CSV), we will use the field note to store the Assyriological abbreviation of the publication. Follow previous abbreviation conventions found here: https://cdli.mpiwg-berlin.mpg.de/abbreviations. 

##### designation (CSV only)
We use this field to store the Assyriological abbreviation of the publication. Follow previous abbreviation conventions found here: https://cdli.mpiwg-berlin.mpg.de/abbreviations. 


#### Bibliographic data formats

#### BibTeX
You can read more about the BiBTeX format on the [official BiBTeX website](http://www.bibtex.org/) and at the [BiBTeX Wikipedia entry](https://de.wikipedia.org/wiki/BibTeX).

#### CSV and TSV

#### JSON

#### RIS

#### TTL and RDF/JSON



<div class="alert alert-info">
More information regarding bibliographic data in cdli will be available in the user guides in the future.
</div>
