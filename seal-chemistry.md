---
title: Seal Chemistry and Calibration form Instruction for Editors
section: Editor Guides
category: Features Guide
audiences:
  - Editors
chips:
  - editors
authors:
  - Singh, Aditi
---
# Seal Chemistry and Calibration form Instruction for Editors

To add chemical information regarding a seal, follow the below steps:

## Create and Submit the Calibration File 

1. Create a CSV file with the following fields - 
    - Standard Name
    - Creator
    - Collaborators
    - Comment
    - Instrument
    - Software
    - Instrument Class
    - Date
    - Time
    - Duration
    - Grade
    - Further columns with element name and error(+/-)

  Example - ![Calibration file CSV](/cdli-docs/images/calibration_csv.png)

2. For element name and its respective error(+/-), add value in percentage format.

3. For Human_id, the input should be museum name_date 


   Eg: `ashmolean_09/11/2022`

4. Send the file to the admin for upload. 

## Create and Submit the seal chemistry File


1. Create a CSV file with the following fields - 
    - P Number - The P number of the seal, Eg: P473511
    - Reading	
    - Area - The area on the seal from where the reading is taken
      
      
      Eg: Bottom, Top, Surface.
          
        if there are more than one readings for a particular surface mention it like 
        Surface 1, Surface 2, Surface 2 etc.
    - Area description - Attributes of the seal
    - Date
    - Time
    - Duration
    - Further columns with element name and error(+/-)

  Example - <br>![Seal Chemistry file](/cdli-docs/images/seal_chemistry_csv.png)

2. Send the file to admin for review along with the associated calibration files.


