---
title: Adding or editing text
section: User guides
category: Text edition
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Tinney, Steve
  - Pagé-Perron, Émilie
---
For now, please use the documentation available at ORACC: http://oracc.museum.upenn.edu/doc/help/editinginatf/cdliatf/index.html
