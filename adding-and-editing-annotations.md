---
title: Adding and editing image annotations
section: User guides
category:
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Willighagen, Lars
  - Dutta, Abhishek
  - Pagé-Perron, Émilie
---

## Adding and editing image annotations

Image annotations can be added or edited from this page: https://cdli.mpiwg-berlin.mpg.de/artifact-asset-annotations/add.
First, upload a file containing annotations (see below for which file formats are supported).
Then, confirm that the file was parsed correctly. Afterwards, add metadata to your annotations
by describing the work that was done, and adding associated authors and projects.

### Manual annotation workflow for CDLI images

To annotate images in CDLI, you need to employ an external image annotator. We prefer to use
the [VGG Image Annotator (VIA)](https://www.robots.ox.ac.uk/~vgg/software/via/) for which the
instructions are below.

1. Load the VIA2 annotator by visiting the following URL: https://www.robots.ox.ac.uk/~vgg/software/via/via.html.
   You can also open an offline copy of via.html in you browser.

2. Click "Project" -> "Load" and point the file selector to the following file: https://cdli.mpiwg-berlin.mpg.de/files-up/cdli-via2-template-2024-04.json.
   This is a VIA2 project JSON file which contains a pre-defined list of names that can be
   manually annotated. You may want to download this file first in your local computer.

3. Click "Project" tab on the left side and then click "Add URL" to add an image URL.
    1. For example, the seal shown at https://cdli.mpiwg-berlin.mpg.de/P530718 has
    the following image URL: https://cdli.mpiwg-berlin.mpg.de/dl/photo/P530718.jpg
    2. If you want to annotate more than one image, enter one URL per line in the lower
       input box.
    3. After clicking the "Ok" button, you will see that the image is now available for
       manual annotation.

4. Draw a rectangular region by pressing mouse button and dragging it. After the region
   is defined, you can describe the content of the region by selecting one of the items
   (e.g. standing figure) from the dropdown menu.
    1. For the CDLI seal annotation project, the name of objects have not been
       standardised yet. The current list of object names is tentative and will get
       updated and standardised in the future.
    2. If you would like to add an a new entry to this list, please contact Jacob Dahl.

5. Save a copy of your work as a VIA2 project by clicking "Project" -> "Save". This will
   save a JSON file that you can use for future reference.

6. To contribute your annotations to the CDLI project, follow these steps.
    1. Go to https://cdli.mpiwg-berlin.mpg.de/artifact-asset-annotations/add
    2. Upload the project JSON file that you saved in step 5. **If you want to replace
       all existing annotations, enable "Overwrite" mode.**
    3. Go through the submission forms.

7. To edit existing annotations of an artifact, follow these steps.
    1. Go to the artifact page, then click "View annotations".
    2. In the annotations viewer, click "Download" in the right sidebar and choose the
       VIA format.
    3. Open the VIA2 annotator (see step 1) and load the project file (see step 2).
    4. Then, click "Annotation" -> "Import annotations (from json)" and upload the file
       downloaded in step 7.2.
    5. Make the changes you want and upload as detailed in steps 5–6.

### Formats supported for importing

#### W3C Web Annotation Data Model (WADM)

JSON imports following the [W3C Web Annotation Data Model](https://www.w3.org/TR/annotation-model/)
are supported, but should conform to the following restrictions, as linked data is not
yet supported in full:

  - All keys should be prefix-less and follow the `@context` used in the examples of the W3C specification.
  - The top-level value can be an `AnnotationCollection` object, an `AnnotationPage object`, or an array or
    object with individual annotations as values. In the case of `AnnotationCollection` objects, only the
    annotations included in the file are imported.
  - Individual annotations should have one and only one `target`. This target should have a
    `source` consisting of the URL of the CDLI image you are annotating (e.g. `/dl/photo/P123456.jpg`).
    This target should have a single `selector` of the type `SvgSelector`. Preferably, use only `<polygon>`,
    `<circle>`, `<ellipse>`, and `<path>`, and do not use attributes like `transform`.
  - Individual annotations should have at least one `body` but may have more. These bodies
    should have the type `TextualBody` or `SpecificResource`. The bodies may have any or no `purpose`.
    Bodies may additionally have a `value` and/or a `source`.
  - Individual annotations may have a `license`.
  - In the case an existing annotation should be updated, the `id` (`@id`) should be the
    URL of the existing annotation (`https://cdli.mpiwg-berlin.mpg.de/artifact-asset-annotations/123`
    where `123` is the numeric ID of the annotation).

The fields `created`, `modified`, and `creator` are not supported for importing, and instead
are filled in from the update event(s) associated with the annotations.

```json
{
  "@context": "http://www.w3.org/ns/anno.jsonld",
  "id": "https://cdli.mpiwg-berlin.mpg.de/artifact-asset-annotations/123",
  "type": "Annotation",
  "body": [
      {
          "type": "TextualBody",
          "value": "crudely inscribed",
          "purpose": "commenting"
      },
      {
          "type": "SpecificResource",
          "value": "vase",
          "source": "https://www.wikidata.org/wiki/Q191851",
          "purpose": "tagging"
      }
  ],
  "target": {
    "source": "https://cdli.mpiwg-berlin.mpg.de/dl/photo/P123456.jpg",
    "selector": {
      "type": "SvgSelector",
      "value": "<svg> ... </svg>"
    }
  }
}
```

#### Common Objects in Context (COCO)

Support for the [Common Objects in Context (COCO) data format](https://cocodataset.org/#format-data)
is pending.

#### VGG Image Annotator (VIA)

Both VIA project files and VIA annotation JSON files can be imported.

  - To edit existing annotations, a text field named `@id` is used containing the full URL
    of the annotation.
  - Four fields are imported, `classifying`, `identifying`, `describing`, and `assessing`,
    corresponding to the W3C purposes of the same name. These fields are expected to be radio buttons
    or checkboxes, with URIs to the [Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
    vocabulary as the option identifiers. If no term is available, the option identifier should be
    a non-URI label.
  - If a VIA project is uploaded, labels corresponding to the URIs will be extracted from there.

## Viewing the resulting annotations

When an update event is accepted, you can click on the link in the column titled "Visual asset" to view
the image and its associated annotations.
