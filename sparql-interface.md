---
title: SPARQL interface
section: User guides
category: Features guide
audiences:
  - Visitor
chips:
  - linked data
  - sparql
authors:
  - Willighagen, Lars
---

A user interface for the [SPARQL](https://en.wikipedia.org/wiki/SPARQL) endpoint is available at [https://cdli.mpiwg-berlin.mpg.de/resources/sparql](https://cdli.mpiwg-berlin.mpg.de/resources/sparql).

## Query

The interface contains three fields:

  - **Endpoint** sets the SPARQL endpoint used for the query.
  - **Display** sets the way the results of the query are displayed (see [Result displays](#content-result-displays)).
  - **Query** is the [SPARQL query](https://www.w3.org/TR/sparql11-query/) that will be executed (see [Examples](#content-examples)).

Additionally, the interface shows a collapsible list of prefixes. These are automatically included in the query, so instead of `<http://www.cidoc-crm.org/cidoc-crm/P87_is_identified_by>` you can write `crm:P87_is_identified_by`.

## Result displays

### Table

The table displays the data as-is, though URIs are prefixed. The results are not paginated, so be sure to limit your query where necessary.

### Network graph

The network graph can be used to show hierarchical structures as well as other relations between data points. Regular nodes are colored in gray, blank nodes in light blue, and literal values in white.

It connects the first parameter to each of the other parameters. Optionally, text can be added to each of those links with the `?edgeLabel` parameter. To display human-readable labels for nodes, include an additional parameter suffixed with `Label` (e.g. `?genre` and `?genreLabel`). URIs in nodes are strongly compacted, but the full URI is visible when hovering over a node.

### Map

The map view can display coordinates and associated data on a geographical map. It takes the parameter `?geometry` for [GeoJSON](https://geojson.org/) strings. Such data is available for the following entities:

  - Collections are connected to their locations with `crm:P53_has_former_or_current_location`. For these locations, `geo:hasCentroid` is available.
  - Proveniences are connected to their locations with `crm:P161_has_spatial_projection / crm:P189i_is_approximated_by` and `pleiades:hasLocation`. For these locations, `geo:hasCentroid` and/or `geo:hasGeometry` is available.
  - Regions are themselves locaations, and have `geo:hasGeometry` available.

Additional fields are displayed in a popup. To display human-readable labels for nodes, include an additional parameter suffixed with `Label` (e.g. `?provenience` and `?provenienceLabel`).

## Examples

### Hierarchical view of genres

```sparql
SELECT * WHERE {
    ?genre a cdli:terms\/type\/genre; rdfs:label ?genreLabel .
    OPTIONAL { ?genre crm:P127_has_broader_term ?parent }
}
```

[Try it out!](https://cdli.mpiwg-berlin.mpg.de/resources/sparql?endpoint=https%3A%2F%2Fcdli.utoronto.ca%2Fsparql&display=graph&query=SELECT%20*%20WHERE%20%7B%0D%0A%20%20%20%20%3Fgenre%20a%20cdli%3Aterms%5C%2Ftype%5C%2Fgenre%3B%20rdfs%3Alabel%20%3FgenreLabel%20.%0D%0A%20%20%20%20OPTIONAL%20%7B%20%3Fgenre%20crm%3AP127_has_broader_term%20%3Fparent%20%7D%0D%0A%7D%0D%0A)

### Map of proveniences associated with artifacts at Harvard University

```sparql
SELECT ?provenience ?provenienceLabel ?geometry (COUNT(?artifact) AS ?count) WHERE {
    VALUES ?collection { cdli:collections\/17 }
    ?artifact crm:P46i_forms_part_of ?collection ;
              crma:AP21i_is_contained_in / crma:AP5i_was_partially_or_totally_removed_by / crm:P7_took_place_at / crm:P89_falls_within ?provenience .
    ?provenience crm:P161i_is_spatial_projection_of / rdfs:label ?provenienceLabel ;
                 crm:P189i_is_approximated_by / geo:hasCentroid / geo:asGeoJSON ?geometry .
} GROUP BY ?provenience ?provenienceLabel ?geometry
```

[Try it out!](https://cdli.mpiwg-berlin.mpg.de/resources/sparql?endpoint=https%3A%2F%2Fcdli.utoronto.ca%2Fsparql&display=map&query=SELECT%20%3Fprovenience%20%3FprovenienceLabel%20%3Fgeometry%20(COUNT(%3Fartifact)%20AS%20%3Fcount)%20WHERE%20%7B%0D%0A%20%20%20%20VALUES%20%3Fcollection%20%7B%20cdli%3Acollections%5C%2F17%20%7D%0D%0A%20%20%20%20%3Fartifact%20crm%3AP46i_forms_part_of%20%3Fcollection%20%3B%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20crma%3AAP21i_is_contained_in%20%2F%20crma%3AAP5i_was_partially_or_totally_removed_by%20%2F%20crm%3AP7_took_place_at%20%2F%20crm%3AP89_falls_within%20%3Fprovenience%20.%0D%0A%20%20%20%20%3Fprovenience%20crm%3AP161i_is_spatial_projection_of%20%2F%20rdfs%3Alabel%20%3FprovenienceLabel%20%3B%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20crm%3AP189i_is_approximated_by%20%2F%20geo%3AhasCentroid%20%2F%20geo%3AasGeoJSON%20%3Fgeometry%20.%0D%0A%7D%20GROUP%20BY%20%3Fprovenience%20%3FprovenienceLabel%20%3Fgeometry)

### Federated query to fetch Wikimedia Commons images associated with artifacts

```sparql
SELECT ?artifact ?wikidata ?image WHERE {
  ?artifact a crm:E22_Human-Made_Object; crm:P1_is_identified_by [
    crm:P37i_was_assigned_by / crm:P14_carried_out_by cdli:external-resources\/267 ;
    crm:P190_has_symbolic_content ?wikidataUri
  ] .
  BIND(IRI(?wikidataUri) AS ?wikidata)
  SERVICE <https://query.wikidata.org/sparql> {
    ?wikidata <http://www.wikidata.org/prop/direct/P18> ?image
  }
}
```

[Try it out!](https://cdli.mpiwg-berlin.mpg.de/resources/sparql?endpoint=https%3A%2F%2Fcdli.utoronto.ca%2Fsparql&display=table&query=SELECT%20%3Fartifact%20%3Fwikidata%20%3Fimage%20WHERE%20%7B%0D%0A%20%20%3Fartifact%20a%20crm%3AE22_Human-Made_Object%3B%20crm%3AP1_is_identified_by%20%5B%0D%0A%20%20%20%20crm%3AP37i_was_assigned_by%20%2F%20crm%3AP14_carried_out_by%20cdli%3Aexternal-resources%5C%2F267%20%3B%0D%0A%20%20%20%20crm%3AP190_has_symbolic_content%20%3FwikidataUri%0D%0A%20%20%5D%20.%0D%0A%20%20BIND(IRI(%3FwikidataUri)%20AS%20%3Fwikidata)%0D%0A%20%20SERVICE%20<https%3A%2F%2Fquery.wikidata.org%2Fsparql>%20%7B%0D%0A%20%20%20%20%3Fwikidata%20<http%3A%2F%2Fwww.wikidata.org%2Fprop%2Fdirect%2FP18>%20%3Fimage%0D%0A%20%20%7D%0D%0A%7D%0D%0A)
