---
title: Adding and editing documentation
section: User guides
category:
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
---
## Setup


## YAML header
The documentation system uses a YAML header to sort and display in the proper places the different documents composing our documentation. This header stores different information using an internal controlled vocabulary.  

Example:   
```
---
title: Adding and editing documentation
section: User guides
category:
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
---

```

### title
The title variable is the title of the document, it must summarize the content of the whole file.

### section
Sections are the broad categorizations for the documentation's content.
- User guides
- Contribution and data guides
- Dev and devops docs

### category
The pre-cited sections can be classified further into categories.
- Search guide
- Browse guide
- Features guide
- Editor guide
- Data formats and data
- Contribution guides (data)
- Contribution guides (code)
- API usage
- Style guides
- Deploy guide
- Features documentation

### audiences
  - Visitor
  - Content contributor
  - Developer
  - Organization

### chips
Chips are tags that can be used to help the users visualize topics of interest discussed in the documentation. They do not belong to a restricted list but are more helpful as they repeat.

### authors  
The full name of each author Last name first, followed by a comma, and then all other names. eg.: Pagé-Perron, Émilie. Each name comes on a separate line and are added in alphabetical order.  

## Files 
- Filename must be explicit
- Filename mut contain dashes, not underscores
- Use the extension .md
- Images go in the /images folder 
  
  Images will not display when the files are looked at from the cdli_docs repo but they appear when rendered on the website. below is an example of the formating to use for images: 
`![alt text for testing](/cdli-docs/images/browse_ss1.png)`
![alt text for testing](/cdli-docs/images/browse_ss1.png)
