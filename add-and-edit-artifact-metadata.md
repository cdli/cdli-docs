---
title: Adding and editing artifact metadata
section: Contribution and data guides
category: Editor guide
audiences:
  - Content contributor
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
---

## Adding a new artifact using the add artifact form
After logging in, you can submit information about a new artifact at this address:
https://cdli.mpiwg-berlin.mpg.de/artifacts/add. For more explanations regarding each field, you can look at the [Artifacts metadata fields](artifact-metadata-fields) documentation page.

## Editing an artifact using the edit artifact form
After logging in, you can propose changes to the metadata of an artifact by clicking the edit button and choosing "Edit Metadata". This button can be found on the artifact's page and on the expanded search results page.  
On the artifact page:  
![csv options](/cdli-docs/images/edit-metadata.png)  

On the expanded search results page:  
![csv options](/cdli-docs/images/edit-metadata2.png)  
For more explanation regarding each field, you can look at the [Artifacts metadata fields](artifact-metadata-fields) documentation page.


## Adding or editing artifact entries using a file
The easiest way to submit multiple new artifacts or to update artifacts is to use a file. When your file is ready, after logging in, you can submit it here: https://cdli.mpiwg-berlin.mpg.de/artifacts-updates/add  

You can use the same link to check the validity of your data without submitting your changes to the update queue. The system will provide you with feedback on your data and let you know if there are any issues.  

The best way to start preparing such a file is to download existing entries to use as an example. To download metadata from one or more artifacts, you can use the export button on the search results page, and the Export artifact button on the single artifact page. Chose the "CSV" option.  

[Download an example csv file](https://cdli.mpiwg-berlin.mpg.de/search?simple-value%5B0%5D=masz-da-re-a&simple-field%5B0%5D=keyword&f%5Bperiod%5D%5B0%5D=Lagash+II+%28ca.+2200-2100+BC%29&format=csv)  
You can submit only the columns that you wish to update.  

When submitting, you can chose to use the "concatenate" option which is used to append new information to existing data in some of the multiple entry fields.  

For a detailed description of each field, please see [Artifacts metadata fields](artifact-metadata-fields)  

### Opening a CDLI metadata csv file
#### With Open or Libre Office
- Start the software and go to File > Open
- Follow the instructions on the screen and chose these options:
    - Character set: Unicode (UTF-8)
    - Separator options: Comma
    - String delimiter: "
- Click OK
![csv options](/cdli-docs/images/csv-options.png)  
By default these options should already be chosen but double-check before clicking "OK"  

#### With Microsoft Excel
- Start the software and go to File > Import
- Choose the option "CSV-data"
- Choose the file and click "Import Data"  

The import assistant will then walk you trough three screens of options:  
![csv options](/cdli-docs/images/excel1of3.png)  
On the first screen, the details will usually be wrong by default. Make sure to select the proper encoding and data delimiters:
- Original data type: Delimited
- File origin: Unicode (UTF-8)  
  
![csv options](/cdli-docs/images/excel2of3.png)    
  
- Delimiters: Comma
- Text qualifier: "

In the third screen:  
- Choose "Standard"
- Click Finish

### Exporting or saving a CDLI metadata csv file
