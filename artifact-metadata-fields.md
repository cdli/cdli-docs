---
title: Artifacts metadata fields
section: Contribution and data guides
category: Editor guide
audiences:
  - Visitor
  - Content contributor
  - Developer
chips:
  - editor
authors:
  - Pagé-Perron, Émilie
  - Englund, Robert K.
---

## Artefact metadata fields overview
|field name| data type|index|
|----------------------------|---|---|
| artifact_id |number from index|[artifacts index](/search)|
| cdli_comments  |text||
| designation |text||
| artifact_type  |text from index|[Artifact types index](/artifact-types)|
| period|text from index|[periods index](/periods)|
| provenience |text from index|[Proveniences index](/proveniences)|
| written_in  |text from index|[Proveniences index](/proveniences)|
| archive  |text from index|[Archives index](/archives)||
| composite_no|text||
| seal_no |text||
| composites|text from index|[Composites index](/search?f%5Bcomposite_seal%5D%5B%5D=composite)|
| seals|text from index|[Seals index](/search?f%5Bcomposite_seal%5D%5B%5D=seal)|
| museum_no|text||
| accession_no|text||
| condition_description|||
| artifact_preservation|||
| period_comments|text||
| provenience_comments |text||
| artifact_type_comments  |text||
| is_provenience_uncertain|binary||
| is_period_uncertain  |binary||
| is_artifact_type_uncertain |binary||
| is_school_text |binary||
| height |decimal ||
| thickness |decimal ||
| width |decimal ||
| weight |decimal ||
| elevation|text||
| excavation_no  |text||
| findspot_square|text||
| findspot_comments |text||
| stratigraphic_level  |text||
| surface_preservation |||
| artifact_comments |text||
| seal_information  |text||
| is_public |binary||
| is_atf_public  |binary||
| are_images_public |binary||
| collections |text from index||
| dates |text||
| alternative_years |text||
| genres|text from index||
| languages|text from index||
| materials|text from index|[Materials index](/materials)|
| shadow_cdli_comments |text from index||
| shadow_collection_location |text||
| shadow_collection_comments |text||
| shadow_acquisition_history |text||
| publications_key  |text from index||
| publications_type |text||
| publications_exact_ref  |text||
| publications_comment |text||
| external_resources|text from index||
| external_resources_key|text|[External resources index](/external-resources)|
| genres_comment |text||
| genres_uncertain  |binary||
| languages_uncertain  |binary||
| materials_aspect |text from index|[Material aspects index](/material-aspects)|
| materials_color|text from index|[Material colors index](/material-colors)|
| materials_uncertain  |binary||
| retired  |binary||
| has_fragments  |binary||
| is_artifact_fake  |binary||
| redirect_artifact_id |number from index|[artifacts index](/search)|

All entities indexes are available here: https://cdli.mpiwg-berlin.mpg.de/browse

## Artefact metadata fields description

 ###  artifact_id

 ###  cdli_comments  

 ###  designation

 ###  artifact_type

 ###  period
 Period designations in CDLI catalogue follow those generally accepted by specialists in cuneiform studies, and consist of a conventional period name followed by approximate dates BC, for instance Uruk III (ca. 3200-3000 BC) or Old Babylonian (ca. 1900-1600 BC).

 ###  provenience
Where known from excavations, or relatively certain from internal considerations, the provenience of a text artifact is given in the format Ancientname (mod. Modern name), for instance Bābili (mod. Babylon), or Kar-Tukulti-Ninurta (mod. Tulul al-ʿAqir). Where one or the other is not known, or neither, it is signaled with “uncertain.” For a more detailed description of this entity, see the [provenience documentation](proveniences).

 ###  written_in
In the case where it is possible to distinguish a place where the artifact was written which is different from the artifact find-spot, this field can be used to specify this information, by recording a place using our proveniences list.

 ###  archive
 In case the archive is known, it can be selected using this field. We cannot accommodate storing information about the dossier yet, but we will in the future.

 ###  composite_no
 Oracc’s Q-catalogue forms the basis for all cuneiform texts liable to to be found in multiple copies with the exception of seals, thus including royal inscriptions, literary and lexical texts, and a variety of sundry other texts. These Q-designations are of the form Q123456 (Q plus six digits), and can be browsed here.  

 The Q number is manually assigned, when the artifact is a composite text, and not a seal. (Expert: [Jamie Novotny](/authors/472))

 ###  seal_no
Like Q, seal numbers are of the form S123456, that is, S(eal) plus six digits. Seal numbers refer in essence exclusively to physical cylinder or stamp seals, even though we may see that final numbers of seals found only in their impressions on ancient clay artifacts roughly equal those of physical seal artifacts found in existing collections. CDLI work put into seals has been described in two CDLN contributions by Englund ([CDLN 2014:4](/articles/cdln/2014-4)) and Firth ([CDLN 2014:26](/articles/cdln/2014-26)), and Mesopotamian seals are currently the focus of a research project being led by CDLI co-director Jacob Dahl at the University of Oxford.

The S number is manually assigned, when the artifact is a physical seal, or a composite seal (not a seal impression). (Expert: [Richard Firth](/authors/981))

 ###  composites
Separated by semicolons, this field should contain the Q number of any composition this artifact is a witness of.

 ###  seals
Separated by semicolons, this field should contain the S number of any seal which has been used to impress the artifact in question.

 ###  museum_no
 Number of the object or fragments in the collection. This normally consists of museum siglum + number. The meaning of collection sigla, usually abbreviations of the museums themselves, is known to specialists, and can usually be deduced from the name found in the “Collection” field, but they can also be found in CDLI’s abbreviations pages. Examples are YBC 14389, or VAT 4126. As with publication numbers, many collection numbers contain leading zeros in the database so that records could be sorted properly in the previous versions of the CDLI interface. In the case of joins of fragments with different collection numbers, the full collection number will be entered for each fragment.

 ###  accession_no
An artifact can receive a variety of identifying designations in the course of moving from ground to collection storage or display. Often, the artifact receives IDs in the field from excavators, then is registered with accession IDs by museums, and is finally assigned a stable collection ID by curators. These are not always widely known, or consistently used identifiers, CDLI catalogue attempts to differentiate between accession and collection IDs, not always successfully. The Kuyunjik (Nineveh) collection of the British Museum has a number of different IDs referring to various excavators (Sm n, Rm n, etc.), to Kuyunjik itself (K n), or as generally with BM pieces, to the date of accession in the museum (1896-06-12, 0035 is the 35th artifact registered on the 12th of June 1896; lecacy preceding zeros are present in this field. The meaning of collection sigla, usually abbreviations of the museums themselves, is known to specialists, and can usually be deduced from the name found in the “Collection” field.

 ###  condition_description
 ###  artifact_preservation
 ###  period_comments
 ###  provenience_comments
 ###  artifact_type_comments  
 ###  is_provenience_uncertainbinary
 ###  is_period_uncertain
 ###  is_artifact_type_uncertain binary
 ###  is_school_ binary
 ###  height (decimal)
This value should be in millimeters, decimals can be added after a period.

 ###  thickness (decimal)
This value should be in millimeters, decimals can be added after a period.
 ###  width (decimal)
 This value should be in millimeters, decimals can be added after a period.
 ###  weight (decimal)
This value should be in grams, decimals can be added after a period.
 ###  elevation
 ###  excavation_no  
 We attempt to follow archaeological conventions in the assignment of excavation numbers to CDLI text artifacts. Such designations can be highly inconsistent. Thus, Uruk/Warka texts are listed, for instance, as W 12345, whereby in early excavations each number represented an artifact “locus” that could itself consist of tens and even hundreds of discrete artifacts. These individual artifacts were, without apparent attention to convention, designated with letters or numbers and sorted alpha-numerically. As with publication numbers, many excavation numbers contain leading zeros in the database so that records could be sorted properly in the previous versions of the CDLI interface.

 ###  findspot_square
 ###  findspot_comments
 ###  stratigraphic_level  
 ###  surface_preservation
 ###  artifact_comments
 ###  seal_information  
 ###  is_publicbinary
 ###  is_atf_public
 ###  are_images_public
 This field is deprecated and will be removed in the future. It contains legacy information about access to images.
 ###  collections
 ###  dates
Beginning in the Early Dynastic period ca. 2400 BC, Babylonian scribes began to qualify administrative and legal texts with notations clearly identifiable as date designations, consisting of all or some of the categories Ruler, Year of rule, Month of year, Day of month. From the Late Uruk period of the latter third of the 4th millennium BC on, these calendars combined knowledge of solar and lunar cycles to achieve an ideal administrative year of 360 days divided into 12 months of 30 days each. The cultic calender evidently was based on the lunar cycle of ca. 29.5 days for each month, and therefore a lunar year of ca. 354 days and thus the need for intercalation of extra months on average every three years. These dates are currently entered to CDLI catalogue in the form RN.Y.M.D (Royal name is spelled in full with conventional English designations), with “--” for lost information, “00” when information was not given by  the scribe. Month intercalations were designated by scribes with "min," “the second,” or "diri," “extra.” A question mark following a space after the full date notation records doubts about any one, or all of the preceding RN.Y.M.D slots.

 ###  alternative_years
If the date is uncertain in terms of the year, you can add here alternative dates with the other possible years based on the year name fragment or other internal information.

 ###  genres
 ###  languages
 ###  materials
 ###  shadow_cdli_comments
 ###  shadow_collection_location
 ###  shadow_collection_comments
 ###  shadow_acquisition_history
 ###  publications_key  
 ###  publications_type
 ###  publications_exact_ref  
 ###  publications_comment
 ###  external_resources
 ###  external_resources_key  
 ###  genres_comment
 ###  genres_uncertain
 ###  languages_uncertain
 ###  materials_aspect  
 ###  materials_color
 ###  materials_uncertain
 ###  retired
 Instead of deleting entries which we do not need anymore, we retire them so we keep a trace of the fact they are not in use anymore and
 ###  has_fragments
 ###  is_artifact_fake
 ###  redirect_artifact_id
artifact id to which the current entry should redirect, only when the entry is retired.


-------------

*Some of the content of this documentation page is recycled from the previous CDLI "Search Aids"*
