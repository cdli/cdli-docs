---
title: Proveniences
section: Contribution and data guides
category: Data formats and data
audiences:
  - Content contributor
chips:
  - editor
  - data consumer
authors:
  - Rattenborg, Rune
---

The present document provides a comprehensive overview of the **provenience** entity type as employed within the framework of the [Cuneiform Digital Library Initiative](cdli.ucla.edu). First is given a [definition](#definition) of the entity type and its relation to related entities within the framework. Then follows an overview of the various record [fields](#fields) stored for this entity, along with subsections for each field giving exhaustive descriptions of their format, content, and application. Then follows an overview of mandatory and optional [links](#links) to external entities relating to the present entity type. The concluding section briefly summmarises the [history](#history) of the present entity type within the [Cuneiform Digital Library Initiative](cdli.ucla.edu) and related projects.


## Definition
Within the [Cuneiform Digital Library Initiative](cdli.ucla.edu), a **provenience** is defined as *any discrete geographic location of a physical, archaeological feature*. These entities typically relate either to artefact provenance, e.g. the origin of an inscribed object, or geographically identifiable elements of historical geographies. A provenience may refer to an extant physical feature, as well as any extinct physical feature whose location can be reconstructed with a high level of spatial accuracy. A physical feature may include anything from an inscription carved on a modestly sized rock to a mounded site extending over several square kilometres.

A **provenience** is then distinguished from an historical **place** by virtue of its existence as a geographical entity. A **provenience** is distinguished from an historical **region** in that the geographical demarcation of the latter is conceptually, rather than physically defined and in that a **region** is a parent entity to - and typically much larger in extent than - a **place**.

A **provenience** can be linked to a **place** record to associate the physical location and the archaeological feature with a historical entity. A provenience can be linked to one or more **name** records to associate various toponyms with the physical location.

Note that a **provenience** is conceptually closely related to a **location** as defined in the [conceptual framework](https://pleiades.stoa.org/help/conceptual-overview) of the [*Pleiades: A Gazetteer of Past Places*](http://pleiades.stoa.org) historical geographical database.

## Fields
name | field_name | type | description
-----|------------|------|-------------
id |
[provenience](#provenience) | ||The name of the provenience
region |
coordinates |
[ancient name](#ancient-name) |
[modern name](#modern-name) |

### Provenience

### Ancient name

### Modern name


## Links
Provenience records should include mandatory links to two external repositories, along with optional links to several others. These are listed below:

format | repository | URL
-------|------------|------
place_id | [Pleiades](http://pleiades.stoa.org) |http://pleiades.stoa.org
item_id | [WikiData](http://wikidata.org) |http://wikidata.org
id | GeoNames |
id | OpenStreetMap |
id | World History Gazetteer |

## History
Provenience records held by the [Cuneiform Digital Library Initiative](cdli.ucla.edu) have been added continuosly since the formation of the catalogue in 2003. The legacy format of provenience records, still in widespread use in other databases, consisted of a single character string with the following format: **'ancient name (mod. [modern name])'**, with association certainty indicated by the absence (certain) or presence (uncertain) of a question mark **'?'** apended to the string. Similar formal conventions underlie the [ANE Site Placemarks](https://doi.org/10.5281/zenodo.6384044) index of Olof Pedersén of [Uppsala University](https://www.lingfil.uu.se/research/assyriology/earth/), but here the notion of certainty indicated by the question mark relates to geographical certainty, rather than certainty of association between an object and a provenience.

The original records table has been reviewed, checked, and augmented from 2020-2022 by the research project [Geomapping Landscapes of Writing (GLoW)](https://lingfil.uu.se/research/assyriology/glow) of Uppsala University, with a presentation and overview of the initial index resulting from these efforts presented in [Rattenborg et al. 2021](https://cdli.ucla.edu/pubs/cdlj/2021/cdlj2021_001.html). Curated versions of the GLoW provenience index [Cuneiform Inscriptions Geographical Site Index (CIGS)](https://doi.org/10.5281/zenodo.4960710) can be found at the open data repository [Zenodo](https://zenodo.org). As part of these efforts, the majority of provenience records have also received polygon vector geometry and a derived centroid point vector geometry.  

See the Vocabulary consortium at https://cdli-gh.github.io/glow_vocabularies/ for more information about standard Assyriological metadata structure and controlled vocabularies.
  
<font size=1>Last updated: 31 March 2022</font>
